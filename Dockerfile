FROM mhart/alpine-node:8

RUN yarn global add serverless

ADD . /usr/myapp
WORKDIR /usr/myapp

RUN yarn install

ENV AWS_ACCESS_KEY_ID='A'
ENV AWS_SECRET_ACCESS_KEY='A'

EXPOSE 3000

CMD [ "npm","run", "start:fixture" ]
