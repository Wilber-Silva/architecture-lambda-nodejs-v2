# Serverless Boilerplate Api

Este repositório serve como base para o desenvolvimento de API utilizando Serverless Framework, NodeJs, MongoDB e AWS.

## Guia
  - Dependências
  - Estrutura do projeto
  - Configuração
  - Execuçao
  - Execução dos testes

## Dependências
Este projeto utiliza diversas ferramentas e plugins para a mesma, segue lista de ferramentas e plugins

### Ferramentas
  - Obrigatórios:
    - NodeJS 8+
    - MongoDB

  - Opcionais:
    - Docker
    - AWS CLI

### Plugins
  - Obrigatórios:
    - Eslint
  - Opcionais:
    - Editorconfig
    - Yaml Support

## Estrutura do projeto
Este projeto contem pastas e arquivos que servem para certos propósitos segue a lista dos mesmos:

### Pastas
```
_/ 'Raiz'
__/__tests__ 'Contem arquivos dos testes unitarios'
__/coverage 'Contem arquivos dos relatórios gerados através dos testes'
__/config 'Contem arquivos de configuração do projeto para deploy e execução'
__/documentation 'Contem arquivos para executar a geração da documentação de integração'
__/fixtures 'Contem arquivos para executar a migração das entidades'
__/models 'Contem modelos de requisição e resposta da API'
__/mongoose 'Contem pasta de modelos e arquivo de conexão com MongoDB'
___/models 'Contem arquivos de definição de schema das entidades'
__/services 'Contem pastas que se referem as entidades'
___/entidade 'Contem arquivos que são as funções deste entidade'
__/utils 'Contem arquivos e funções utilitárias'
```

## Configuração
Este projeto pode ser executado localmente e também através de um container docker mas de ante mão sera necessário configura-lo, é necessário configurar seu arquivo;.env.app.yml que se encontra na pasta config.

## Execução
Para executar no ambiente desejado siga o passo para o mesmo.

Local:
```
npm install serverless -g
npm install
npm start
```

Docker:
```
docker-compose build
docker-compose up -d
```

## Execução de testes
Para executar os testes siga estes passos.

Com Relatório:
```
npm run test:coverage
```

Sem Relatório:
```
npm test
```