const mongoose = require('mongoose')
const mockingoose = require('mockingoose').default
const faker = require('faker/locale/pt_BR')
const connection = require('../../mongoose/connection')
const { handler } = require('../../services/users/create')
const { generate } = require('../../utils/generate-cpf')

jest.mock('../../mongoose/connection')

describe('Testing function Create', () => {
  let createConnectionMock

  const name = faker.name.firstName()
  const birthDate = faker.date.past()
  const _id = new mongoose.Types.ObjectId().toHexString()
  const cpf = generate(false)

  const userValid = {
    _id,
    name,
    birthDate,
    cpf
  }

  const context = {}

  beforeAll(() => {
    createConnectionMock = connection.createConnection = jest.fn()
    createConnectionMock.mockImplementation(() => Promise.resolve())
  })

  it('testing all valid', async (done) => {
    const event = { body: JSON.stringify(userValid) }
    const response = { _id }

    mockingoose.users.toReturn(userValid, 'save')
    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 201)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('data', response)
    done()
  })

  it('testing cpf invalid', async (done) => {
    const name = faker.name.firstName()
    const birthDate = faker.date.past()
    const _id = new mongoose.Types.ObjectId().toHexString()
    const cpf = '11111111111'

    const doc = {
      _id,
      name,
      birthDate,
      cpf
    }

    const event = { body: JSON.stringify(doc) }

    mockingoose.users.toReturn(doc, 'save')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 500)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', 'CPF invalid')
    done()
  })

  it('testing error', async (done) => {
    const context = {}
    const event = { body: JSON.stringify(userValid) }

    mockingoose.users.toReturn(new Error('Timeout'), 'save')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 500)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', 'Timeout')
    done()
  })
})
