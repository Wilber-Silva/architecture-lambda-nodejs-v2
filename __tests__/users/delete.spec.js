const mockingoose = require('mockingoose').default
const mongoose = require('mongoose')
const faker = require('faker/locale/pt_BR')
const connection = require('../../mongoose/connection')
const { handler } = require('../../services/users/delete')
const { generate } = require('../../utils/generate-cpf')

jest.mock('../../mongoose/connection')

describe('Testing function Delete', () => {
  let createConnectionMock

  const context = {}

  const name = faker.name.firstName()
  const birthDate = faker.date.past()
  const _id = new mongoose.Types.ObjectId().toHexString()
  const cpf = generate(false)

  const userValid = {
    _id,
    name,
    birthDate,
    cpf
  }

  beforeAll(() => {
    createConnectionMock = connection.createConnection = jest.fn()
    createConnectionMock.mockImplementation(() => Promise.resolve())
  })

  it('testing with user exists', async (done) => {
    const event = {
      body: JSON.stringify(userValid),
      pathParameters: { id: _id }
    }

    mockingoose.users.toReturn(userValid, 'findOneAndUpdate')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 200)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('_id', _id)
    done()
  })

  it('testing with user not exists', async (done) => {
    const event = {
      body: JSON.stringify({}),
      pathParameters: { id: _id }
    }

    const doc = null
    mockingoose.users.toReturn(doc, 'findOneAndUpdate')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 404)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', `User id: ${_id} not found`)
    done()
  })

  it('testing error', async (done) => {
    const event = {
      body: JSON.stringify({}),
      pathParameters: { id: _id }
    }

    mockingoose.users.toReturn(new Error('Timeout'), 'findOneAndUpdate')

    const result = await handler(event, context)
    expect(result).toHaveProperty('statusCode', 500)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', 'Timeout')
    done()
  })
})
