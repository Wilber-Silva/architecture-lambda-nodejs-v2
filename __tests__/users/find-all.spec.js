const mongoose = require('mongoose')
const faker = require('faker/locale/pt_BR')
const mockingoose = require('mockingoose').default
const connection = require('../../mongoose/connection')
const { handler } = require('../../services/users/find-all')
const { generate } = require('../../utils/generate-cpf')

jest.mock('../../mongoose/connection')

describe('Testing function Find-All', () => {
  let createConnectionMock

  const name = faker.name.firstName()
  const birthDate = faker.date.past().toISOString()
  const _id = new mongoose.Types.ObjectId().toHexString()
  const cpf = generate(false)

  const userValid = {
    _id,
    name,
    birthDate,
    cpf
  }

  const context = {}

  beforeAll(() => {
    createConnectionMock = connection.createConnection = jest.fn()
    createConnectionMock.mockImplementation(() => Promise.resolve())
  })

  it('testing without params', async (done) => {
    const doc = [ userValid ]

    const response = {
      docs: [ userValid ],
      limit: 10,
      page: 1,
      pages: 1
    }

    const event = { }

    mockingoose.users.toReturn(doc, 'find')
    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 200)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toEqual(response)
    done()
  })

  it('testing with params', async (done) => {
    const event = {
      queryStringParameters: {
        limit: 10,
        page: 1,
        sort: '-name'
      }
    }

    const doc = [ userValid ]

    const response = {
      docs: [ userValid ],
      limit: 10,
      page: 1,
      pages: 1
    }

    mockingoose.users.toReturn(doc, 'find')
    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 200)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toEqual(response)
    done()
  })

  it('testing error', async (done) => {
    const event = { queryStringParameters: {} }

    mockingoose.users.toReturn(new Error('Timeout'), 'find')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 500)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', 'Timeout')
    done()
  })
})
