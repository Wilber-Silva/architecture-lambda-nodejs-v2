const mockingoose = require('mockingoose').default
const mongoose = require('mongoose')
const faker = require('faker/locale/pt_BR')
const connection = require('../../mongoose/connection')
const { handler } = require('../../services/users/find-one')
const { generate } = require('../../utils/generate-cpf')

jest.mock('../../mongoose/connection')

describe('Testing function Find-One', () => {
  let createConnectionMock

  const name = faker.name.firstName()
  const birthDate = faker.date.past().toISOString()
  const _id = new mongoose.Types.ObjectId().toHexString()
  const cpf = generate(false)

  const userValid = {
    _id,
    name,
    birthDate,
    cpf
  }

  const context = {}

  const event = {
    pathParameters: {
      id: _id
    }
  }

  beforeAll(() => {
    createConnectionMock = connection.createConnection = jest.fn()
    createConnectionMock.mockImplementation(() => Promise.resolve())
  })

  it('testing without params', async (done) => {
    mockingoose.users.toReturn(userValid, 'findOne')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 200)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('data', userValid)
    done()
  })

  it('testing user not found', async (done) => {
    const doc = null
    mockingoose.users.toReturn(doc, 'findOne')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 404)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', `User id: ${_id} not found`)
    done()
  })

  it('testing error', async (done) => {
    mockingoose.users.toReturn(new Error('Timeout'), 'findOne')

    const result = await handler(event, context)

    expect(result).toHaveProperty('statusCode', 500)
    expect(result).toHaveProperty('body')
    expect(JSON.parse(result.body)).toHaveProperty('message', 'Timeout')
    done()
  })
})
