const { handler } = require('../../utils/authorizer')

describe('Testing function authorizer', () => {
  it('testing unauthorized', async (done) => {
    const event = { authorizationToken: null }

    try {
      await handler(event)
    } catch (error) {
      expect(error.message).toEqual('Unauthorized')
      done()
    }
  })

  it('testing generate policy', async (done) => {
    const event = { authorizationToken: 'Bearer' }

    const result = await handler(event)
    expect(result).toHaveProperty('principalId')
    expect(result).toHaveProperty('context')
    expect(result).toHaveProperty('policyDocument')
    done()
  })
})
