const { ResponseError, ResponseNotContent, ResponseNotFoundException } = require('will-core-lib/http')
const userRepository = require('./../../repositories/user-repository')

module.exports.handler = async ({ pathParameters: params }) => {
  try {
    const { id } = params.id
    const user = await userRepository.findById(id)

    if (!user) throw new ResponseNotFoundException('User Not Found')

    await userRepository.delete(user)
    return new ResponseNotContent()
  } catch (error) {
    console.log('error', error)
    return new ResponseError(error)
  }
}
