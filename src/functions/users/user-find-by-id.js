const { ResponseError, ResponseOK, ResponseNotFoundException } = require('will-core-lib/http')
const userRepository = require('./../../repositories/user-repository')

module.exports.handler = async ({ pathParameters: params }) => {
  console.log('path', params)

  try {
    const { id } = params
    const user = await userRepository.findById(id)

    if (user) return new ResponseOK(user)
    throw new ResponseNotFoundException('User Not Found')
  } catch (error) {
    console.log('error', error)
    return new ResponseError(error)
  }
}
