const { ResponseOK, ResponseError } = require('will-core-lib/http')
const userRepository = require('./../../repositories/user-repository')

module.exports.handler = async ({ queryStringParameters: query }) => {
  try {
    const users = userRepository.paginate({ ...query })
    return new ResponseOK(users)
  } catch (error) {
    console.log('error', error)
    return new ResponseError(error)
  }
}
