const { JoiValidation } = require('will-core-lib/validators')
const userSchema = require('./../../validators/user-schema')
const {
  ResponseError,
  ResponseNotContent,
  ResponseNotFoundException,
  ResponseBadRequestException } = require('will-core-lib/http')
const userRepository = require('./../../repositories/user-repository')

module.exports.handler = async ({ pathParameters: params, body }) => {
  try {
    body = JSON.parse(body)
    const validation = await JoiValidation(userSchema.update, body)
    if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

    await userRepository.update({ _id: await userRepository.convertToObjectId(params.id) }, body)
    return new ResponseNotContent()
  } catch (error) {
    console.log('error', error)
    if (error instanceof ResponseNotFoundException) {
      return new ResponseError(new ResponseNotFoundException('User Not Found'))
    }
    return new ResponseError(error)
  }
}
