const { MongoConnection, BaseRepository } = require('will-core-lib/connectors/mongodb')
const userModel = require('./../models/user-model')

class UserRepository extends BaseRepository {
  constructor () {
    super(userModel, new MongoConnection(process.env.CLUB_MONGODB))
  }
}

module.exports = new UserRepository()
