const Joi = require('@hapi/joi')
// Joi.cpf = require('will-core-lib/validators/cpf')
Joi.objectId = require('joi-objectid')(Joi)

const id = Joi.objectId()
const email = Joi.string().email().trim()
const password = Joi.string().min(6).max(8).required()
const firstName = Joi.string().min(3).trim()
const lastName = Joi.string().min(3).trim()
const cpf = Joi.string()
const occupationSlug = Joi.string().min(3).trim()

const aboutUs = Joi.object({
  name: Joi.raw(),
  cnpj: Joi.raw(),
  email: Joi.raw(),
  telephone: Joi.raw(),
  cep: Joi.raw(),
  address: Joi.raw(),
  numberAddress: Joi.raw(),
  neighborhood: Joi.raw(),
  complement: Joi.raw(),
  state: Joi.raw(),
  city: Joi.raw(),
  site: Joi.raw()
})

const aboutMe = Joi.object({
  resume: Joi.raw(),
  from: Joi.raw(),
  gender: Joi.string(),
  birthday: Joi.raw(),
  address: Joi.string(),
  numberAddress: Joi.string(),
  neighborhood: Joi.string(),
  complement: Joi.raw(),
  state: Joi.string(),
  city: Joi.string()
})

const contact = Joi.object({
  email,
  telephone: Joi.string(),
  cep: Joi.string()
})

module.exports.login = Joi.object().keys({
  email: email.required(),
  password
})

module.exports.changePassWord = Joi.object().keys({
  id,
  password,
  confirmationCode: Joi.string().min(6).max(6).trim().required()
})

module.exports.confirmAccount = Joi.object().keys({
  id,
  password,
  confirmationCode: Joi.string().min(6).max(6).trim().required()
})

module.exports.update = Joi.object().keys({
  email,
  firstName,
  lastName,
  cpf,
  aboutUs,
  aboutMe,
  contact,
  occupationSlug,
  partners: Joi.object({
    ipremi: Joi.object({
      userIdExternal: Joi.raw()
    })
  })
})
